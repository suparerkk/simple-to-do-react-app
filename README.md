# Simple To-do React App

[![pipeline status](https://gitlab.com/suparerkk/simple-to-do-react-app/badges/gh-pages/pipeline.svg)](https://gitlab.com/suparerkk/simple-to-do-react-app/-/commits/gh-pages)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).<br/>
Check out [Live Version](https://suparerkk.gitlab.io/simple-to-do-react-app/). (Deployed to [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/#overview) using Gitlab CI/CD)<br/>

![Example](https://gitlab.com/suparerkk/simple-to-do-react-app/-/raw/master/public/sample.gif)

## Features

- Fetch to-do(s) from [this API](https://jsonplaceholder.typicode.com/todos)
- Data saved through local storage
- Mark as Completed/Uncompleted
- Instant Search by title
- Responsiveness

## Instructions

In the project directory, you can run:

`yarn start`

Runs the app in the development mode.<br/>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.<br/>
The page will reload if you make edits.<br/>
You will also see any lint errors in the console.

`yarn build`

Before you run build, please check config.js file for configurations

| variable                 | type      | options          | description                                                                                                     |
| ------------------------ | --------- | ---------------- | --------------------------------------------------------------------------------------------------------------- |
| isDevelopmentEnvironment | `boolean` | `true` , `false` | Used to enable or disable [Redux DevToolsExtension and Redux Logger](https://github.com/reduxjs/redux-devtools) |

This project has been set to deploy on subdirectory, please check package.json for `homepage` and .gitlab-ci.yml for `PUBLIC_URL`

See the section about [react deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## How this application works

- When user access application for the first time
  - application fetch data from [this API](https://jsonplaceholder.typicode.com/todos).
  - application show the result filtered by `visibilityFilters`.
  - `visibilityFilters` can be change to All, Completed or Uncompleted.
  - application will save last `visibilityFilters` user selected in local storage
- When user access application later.
  - application get `todos` and `visibilityFilters` from local storage.
  - application show the result filtered by `visibilityFilters`.
- When user start typing in search input.
  - `todos` will get filtered with search input.
  - user can exit search by click on `X Mark` or press `esc` on keyboard.
- User can reset data by click `RESET` in the top right corner of the page, application will reset and fetch new data from API.

## Related Documentations

### Core

- [React](https://reactjs.org/docs/) JavaScript library for building user interfaces.
- [Redux](https://redux.js.org/introduction/getting-started) State Management.
- [React Redux](https://react-redux.js.org/introduction/quick-start) Redux UI binding library for React.
- [Redux Persist](https://github.com/rt2zz/redux-persist) Persist and rehydrate a redux store.
- [Redux Thunk](https://github.com/reduxjs/redux-thunk) Thunk middleware for Redux.
- [Redux Logger](https://github.com/LogRocket/redux-logger) Logger for Redux.
- [Redux DevTools](https://github.com/reduxjs/redux-devtools) Developer Tools to power-up Redux development.
- [axios](https://github.com/axios/axios) Promise based HTTP client.

### Beautifications

- [smooth-scrollbar](https://github.com/idiotWu/smooth-scrollbar) Momentum based scrolling.

_`Build with ♥ by Suparerk`_
