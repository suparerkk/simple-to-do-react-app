import axios from "axios";

// ACTIONS TYPE'

export const SET_VISIBILITY_FILTER = "SET_VISIBILITY_FILTER";

export const TOGGLE_TODO = "TOGGLE_TODO";
export const FETCH_TODO = "FETCH_TODO";
export const UPDATE_TODO = "UPDATE_TODO";

export const SET_SEARCH_KEYWORD = "SET_SEARCH_KEYWORD";

export const SET_LOADING = "SET_LOADING";
export const SET_ERROR = "SET_ERROR";
export const CLEAR_ERROR = "CLEAR_ERROR";

// OTHER CONSTANTS

export const VisibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_COMPLETED: "SHOW_COMPLETED",
  SHOW_UNCOMPLETED: "SHOW_UNCOMPLETED",
};

export const Ui = {
  REQUEST_IN_PROGRESS: "REQUEST_IN_PROGRESS",
  REQUEST_SUCCESS: "REQUEST_SUCCESS",
};

// ACTION CREATOR

export const setVisibilityFilter = (filter) => {
  return {
    type: SET_VISIBILITY_FILTER,
    filter,
  };
};

export const toggleTodo = (id) => {
  return { type: TOGGLE_TODO, id };
};

export const fetchTodos = () => {
  console.log("accio to-dos!");
  return function (dispatch) {
    dispatch(clearError());
    dispatch(setLoading(true));
    return axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then(({ data }) => {
        dispatch(updateTodos(data));
        dispatch(setLoading(false));
      })
      .catch(function (error) {
        console.log(error);
        dispatch(setError(error));
        dispatch(updateTodos([]));
        dispatch(setLoading(false));
      });
  };
};

const updateTodos = (data) => {
  return { type: UPDATE_TODO, payload: data };
};

export const setSearchKeyword = (keyword) => {
  return { type: SET_SEARCH_KEYWORD, payload: keyword };
};

export const setLoading = (status) => {
  return { type: SET_LOADING, payload: status };
};

const setError = (error) => {
  return { type: SET_ERROR, payload: error };
};

const clearError = (status) => {
  return { type: CLEAR_ERROR };
};
