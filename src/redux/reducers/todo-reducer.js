import {
  TOGGLE_TODO,
  UPDATE_TODO,
  SET_VISIBILITY_FILTER,
  VisibilityFilters,
  SET_LOADING,
  SET_ERROR,
  CLEAR_ERROR,
  Ui,
  SET_SEARCH_KEYWORD,
} from "../actions/todo-action";

export const searchReducer = (state = "", action) => {
  switch (action.type) {
    case SET_SEARCH_KEYWORD:
      return action.payload;
    default:
      return state;
  }
};

export const visibilityFilterReducer = (
  state = VisibilityFilters.SHOW_ALL,
  action
) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return action.filter;
    default:
      return state;
  }
};

export const todosReducer = (state = [], action) => {
  switch (action.type) {
    case TOGGLE_TODO:
      return state.map((todo) =>
        todo.id === action.id ? { ...todo, completed: !todo.completed } : todo
      );
    case UPDATE_TODO:
      return (state = [...action.payload]);
    default:
      return state;
  }
};

const INITAIL_UI_REDUCER = {
  loading: Ui.REQUEST_IN_PROGRESS,
  error: { status: false, details: null },
};

export const uiReducer = (state = INITAIL_UI_REDUCER, action) => {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, loading: action.payload };
    case SET_ERROR:
      return { ...state, error: { status: true, details: action.payload } };
    case CLEAR_ERROR:
      return { ...state, error: { status: false, details: null } };
    default:
      return state;
  }
};
