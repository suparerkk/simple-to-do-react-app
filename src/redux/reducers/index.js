import { combineReducers } from "redux";
import {
  todosReducer,
  visibilityFilterReducer,
  searchReducer,
  uiReducer,
} from "./todo-reducer";

const rootReducer = combineReducers({
  todos: todosReducer,
  visibilityFilter: visibilityFilterReducer,
  search: searchReducer,
  ui: uiReducer,
});

export default rootReducer;
