import { createStore, applyMiddleware, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";
import { createLogger } from "redux-logger";
import { isDevelopmentEnvironment } from "../../config";

import rootReducer from "../reducers";

const enhancers = [];
const logger = createLogger();
const middleware = [thunk];

const persistConfig = {
  key: "root",
  storage,
  blacklist: ["search"],
};

if (isDevelopmentEnvironment) {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

  if (typeof devToolsExtension === "function") {
    enhancers.push(devToolsExtension());
    middleware.push(logger);
  }
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = compose(applyMiddleware(...middleware), ...enhancers);
export const store = createStore(persistedReducer, composeEnhancers);
export const persistor = persistStore(store);
