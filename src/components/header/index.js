import React from "react";
import { connect } from "react-redux";
import { setSearchKeyword, fetchTodos } from "../../redux/actions/todo-action";
import "./header.css";

function Header({ dispatch, props }) {
  /*
   *   handle search input
   */
  const handleChange = (event) => {
    dispatch(setSearchKeyword(event.target.value));
    var XMark = document.getElementById("xmark-for-search");
    XMark.classList.add("visible");
  };

  /*
   *   clear search on key press and click x mark
   */
  const clearSearch = () => {
    dispatch(setSearchKeyword(""));
    var XMark = document.getElementById("xmark-for-search");
    XMark.classList.remove("visible");
  };

  return (
    <div className="App-Header">
      <div id="header-wrapper" className="container">
        <div className="logo text-l">
          To-Do
          <span onClick={() => dispatch(fetchTodos())}>RESET</span>
        </div>
        <div id="search-wrapper" className="text-m">
          <div id="search-box" className="">
            <input
              type="text"
              id="search"
              className="text-m"
              name="search"
              autoComplete="off"
              placeholder="Search"
              value={props.search}
              onChange={(event) => handleChange(event)}
              onKeyDown={(event) => {
                if (event.keyCode === 27) {
                  event.target.blur();
                  clearSearch();
                }
              }}
            />
            <div
              id="xmark-for-search"
              alt="close button"
              onClick={() => clearSearch()}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z" />
              </svg>
            </div>
          </div>
          {props.search.length > 0 ? (
            <div className="tip">
              Press<span>esc</span>to exit search
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    props: { search: state.search },
  };
};

export default connect(mapStateToProps)(Header);
