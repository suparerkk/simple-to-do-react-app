import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
  fetchTodos,
  VisibilityFilters,
  setVisibilityFilter,
} from "../../redux/actions/todo-action";
import "./body.css";

import LoadingIcon from "../../assets/icons/loading-icon";
import Todo from "../todo";

function Body({ dispatch, props }) {
  /*
   *   filter to-dos with search-keyword & visibility filter
   */

  const getVisibleTodos = (todos, filter, search) => {
    if (search !== "") {
      var search_result = [];
      for (let i = 0; i < todos.length; i++) {
        let todoTitle = todos[i].title.toLowerCase();
        let searchKey = search.toLowerCase();
        if (todoTitle.includes(searchKey)) {
          search_result.push(todos[i]);
        }
      }
      return search_result;
    } else {
      switch (filter) {
        case VisibilityFilters.SHOW_ALL:
          return todos;
        case VisibilityFilters.SHOW_COMPLETED:
          return todos.filter((item) => item.completed);
        case VisibilityFilters.SHOW_UNCOMPLETED:
          return todos.filter((item) => !item.completed);
        default:
          throw new Error("Unknown filter: " + filter);
      }
    }
  };

  /*
   *   render to-dos
   */

  const Todos = () => {
    const filteredTodos = getVisibleTodos(
      props.todos,
      props.active,
      props.search
    );
    const todoItems = filteredTodos.map((item, index) => (
      <Todo data={item} key={index} keyItem={index} />
    ));
    return (
      <ul id="todos" className={"text-m loading-" + props.loading}>
        {todoItems}
      </ul>
    );
  };

  /*
   *  check if to-dos is fetched?
   *  if it's not then go fetch!
   */

  const initTodos = () => {
    if (props.todos.length <= 0) {
      dispatch(fetchTodos());
    }
  };

  useEffect(() => {
    initTodos();
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="App-body container">
      <div id="tab" className="text-m">
        <div
          className={props.search.length > 0 ? "search show" : "search hidden"}
        >
          Search for "{props.search}"
        </div>
        <div
          className={props.active === "SHOW_ALL" ? "active" : null}
          onClick={() =>
            dispatch(setVisibilityFilter(VisibilityFilters.SHOW_ALL))
          }
        >
          All
        </div>
        <div
          className={props.active === "SHOW_COMPLETED" ? "active" : null}
          onClick={() =>
            dispatch(setVisibilityFilter(VisibilityFilters.SHOW_COMPLETED))
          }
        >
          Complete
        </div>
        <div
          className={props.active === "SHOW_UNCOMPLETED" ? "active" : null}
          onClick={() =>
            dispatch(setVisibilityFilter(VisibilityFilters.SHOW_UNCOMPLETED))
          }
        >
          Uncompleted
        </div>
      </div>
      <div id="loader" className={"loading-" + props.loading}>
        <LoadingIcon />
      </div>
      <div
        id="app-error"
        className={"alert loading-" + props.loading + " " + props.error.status}
      >
        {props.error.status ? props.error.details.message : null}
        <br />
        Please check your internet connection and try again
      </div>
      <Todos />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    props: {
      todos: state.todos,
      active: state.visibilityFilter,
      search: state.search,
      loading: state.ui.loading,
      error: state.ui.error,
    },
  };
};

export default connect(mapStateToProps)(Body);
