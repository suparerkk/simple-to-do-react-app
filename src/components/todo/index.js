import React from "react";
import { connect } from "react-redux";
import { toggleTodo, VisibilityFilters } from "../../redux/actions/todo-action";

import "./todo.css";

function Todo(props) {
  /*
   *  Toggle to-do Uncomplete <-> Completed
   */
  const switchTodo = () => {
    console.log(props);
    if (props.active !== VisibilityFilters.SHOW_ALL && props.search === "") {
      const allTodos = document.querySelectorAll(".todo-item");
      let offsetHeight = 0;
      if (props.keyItem === 0) {
        offsetHeight = allTodos[props.keyItem].offsetHeight - 4;
      } else {
        offsetHeight = allTodos[props.keyItem].offsetHeight;
      }
      for (let i = props.keyItem; i < allTodos.length; i++) {
        allTodos[i].style.transform = "translateY(-" + offsetHeight + "px)";
      }
      allTodos[props.keyItem].style.opacity = 0;
      setTimeout(() => {
        props.updateTodo(props.data.id);
      }, 250);
    } else {
      props.updateTodo(props.data.id);
    }
  };
  const status = props.data.completed ? "completed" : "uncompleted";
  return (
    <li className={"todo-item text-s " + status}>
      <div className={"toggle-button " + status} onClick={() => switchTodo()}>
        <svg
          className="completed-icon"
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-1.25 16.518l-4.5-4.319 1.396-1.435 3.078 2.937 6.105-6.218 1.421 1.409-7.5 7.626z" />
        </svg>
        <svg
          className="mark-as-complete"
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm6.25 8.891l-1.421-1.409-6.105 6.218-3.078-2.937-1.396 1.436 4.5 4.319 7.5-7.627z" />
        </svg>
        <svg
          className="circle"
          xmlns="http://www.w3.org/2000/svg"
          width="24"
          height="24"
          viewBox="0 0 24 24"
        >
          <path d="M12 0c-6.623 0-12 5.377-12 12s5.377 12 12 12 12-5.377 12-12-5.377-12-12-12zm0 22c-5.519 0-10-4.48-10-10 0-5.519 4.481-10 10-10 5.52 0 10 4.481 10 10 0 5.52-4.48 10-10 10z" />
        </svg>
      </div>
      <div className={"desc " + status}>
        <div className="id">{props.data.id}</div>
        <div>{props.data.title}</div>
      </div>
    </li>
  );
}

const mapDispatchToProps = {
  updateTodo: toggleTodo,
};
const mapStateToProps = (state) => {
  return { active: state.visibilityFilter, search: state.search };
};
export default connect(mapStateToProps, mapDispatchToProps)(Todo);
