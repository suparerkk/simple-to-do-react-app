import React, { useEffect } from "react";
import Scrollbar from "smooth-scrollbar";
import {
  SoftScrollPlugin,
  DisableScrollPlugin,
} from "./assets/plugins/smooth-scroll-plugins";
import "./App.css";

import Header from "./components/header";
import Body from "./components/body";

function App() {
  useEffect(() => {
    /*
     *   init smooth-scrollbar with plugins
     */
    Scrollbar.use(DisableScrollPlugin);
    Scrollbar.use(SoftScrollPlugin);
    Scrollbar.init(document.querySelector(".App"), {
      damping: 0.1,
      continuousScrolling: false,
      overscrollEffect: false,
    });

    return () => {
      Scrollbar.destroyAll();
    };
  }, []);
  return (
    <div className="App">
      <Header />
      <Body />
    </div>
  );
}

export default App;
